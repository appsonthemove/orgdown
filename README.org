* Orgdown

[[file:logo/Orgdown%20logo%20-%2064x64%20transparent%20background.png]]

*Orgdown* ([[doc/faq/name-logo.org][in short "OD]]) is a [[https://en.wikipedia.org/wiki/Lightweight_markup_language][lightweight markup language]] similar to
[[https://en.wikipedia.org/wiki/Markdown][Markdown]] but it's consistent, [[doc/Learning-Orgdown.org][easy to learn]], simple to type even
without [[doc/Tool-Support.org][tool-support]], and it is based on its older brother: [[doc/Org-mode.org][Org-mode]]

The purpose of this site is to provide basic information on the
Orgdown [[doc/Orgdown1-Syntax-Examples.org][syntax]], supported [[doc/Tool-Support.org][software programs, mobile apps,
services]] and [[doc/Parsers.org][parsers]].

** Orgdown levels

There is currently one initial level one named *Orgdown1*.

It covers [[doc/Orgdown1-Syntax-Examples.org][the most basic set of syntax elements]] that any tool has to
support in order to support Orgdown.

Multiple levels of Orgdown with more syntax elements [[doc/Roadmap.org][might be defined
in the future]] if Orgdown1 is getting more and more popular.

Learn more about Orgdown levels on [[doc/Orgdown-Levels.org][this page]].

** How to start with Orgdown

You can take a quick look at [[doc/Orgdown1-Syntax-Examples.org][the Orgdown1 syntax]] to get an overview on
how to emphasize text, create a structure with headings, add links,
write tables, and so forth.

Then you should read [[doc/Learning-Orgdown.org][this page on learing Orgdown]] to start your journey.

You are interested in known Orgdown support in software tools? Visit
[[doc/Tool-Support.org][the tool support page]] for a list.

If you do have some questions, [[doc/FAQs.org][the frequently asked questions]] might be
able to help you.

** Contribute!

You do find the idea of Orgdown intruiging and want to contribute?
This is possible on many different levels [[doc/Contribute.org][starting from promoting the
idea up to advanded topics like (E)BNF or LSP]].

* License

This site is licensed under [[https://creativecommons.org/licenses/by-sa/4.0/][Creative Commons — Attribution-ShareAlike
4.0 International • — CC BY-SA 4.0]] if not stated otherwise.
